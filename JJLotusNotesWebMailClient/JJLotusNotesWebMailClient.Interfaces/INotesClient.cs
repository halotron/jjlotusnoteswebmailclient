﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJLotusNotesWebMailClient.Interfaces
{
    public interface INotesClient
    {
        string UserName { get; set; }
        string Password { get; set; }
        string NotesMailWebServerAddress { get; set; }

        int WaitNumberOfMinutesBetweenUpdates { get; set; }

        int CheckNumberOfUnreadEmailsInInbox();

        void ReadSettings();
        void WriteSettings();

        int PopupDuration { get; set; }
    }
}
