﻿using JJLotusNotesWebMailClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JJLotusNotesWebMailClient
{
    public class NotesClientRandom : INotesClient
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public string NotesMailWebServerAddress { get; set; }

        public int WaitNumberOfMinutesBetweenUpdates { get { return 1; } set { } }

        public int CheckNumberOfUnreadEmailsInInbox()
        {
            Random rand = new Random();
            var nr = rand.Next(10);
            return nr;
        }


        public void ReadSettings()
        {
        }

        public void WriteSettings()
        {
        }

        public int PopupDuration { get { return 1200; } set { } }
    }
}
