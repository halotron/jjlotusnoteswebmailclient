﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace JJLotusNotesWebMailClient
{
    public class BrowserSession
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public BrowserSession()
        {
            DoUrlEncodeFormParameterValues = true;
        }

        private bool _isPost;
        private bool _isDownload;
        private HtmlDocument _htmlDoc;
        private string _download;

        /// <summary>
        /// System.Net.CookieCollection. Provides a collection container for instances of Cookie class 
        /// </summary>
        public CookieCollection Cookies { get; set; }

        /// <summary>
        /// Provide a key-value-pair collection of form elements 
        /// </summary>
        public FormElementCollection FormElements { get; set; }

        private HeaderElementCollection _headerElements;
        public HeaderElementCollection HeaderElements {
            get
            {
                if (_headerElements == null)
                {
                    _headerElements = new HeaderElementCollection();
                }
                return _headerElements;
            }
        }

        /// <summary>
        /// Makes a HTTP GET request to the given URL
        /// </summary>
        public string Get(string url)
        {
            if(string.IsNullOrEmpty(url) || url.Length < 9)
            {
                return "";
            }
            _isPost = false;
            CreateWebRequestObject().Load(url);
            return _htmlDoc.DocumentNode.InnerHtml;
        }

        public bool DoUrlEncodeFormParameterValues { get; set; }

        /// <summary>
        /// Makes a HTTP POST request to the given URL
        /// </summary>
        public string Post(string url)
        {
            try
            {
                _isPost = true;
                var reqObj = CreateWebRequestObject();
                reqObj.Load(url, "POST");



            }
            catch (Exception e)
            {
                var asdf = e;
            }

            var docNode = _htmlDoc.DocumentNode;

            try
            {
                return docNode.InnerHtml;
            } catch(Exception)
            {
                //var ownerDoc = docNode.OwnerDocument;
                //return docNode.InnerText;
                return LatestResponseBody;
            }
        }

        public string GetDownload(string url)
        {
            _isPost = false;
            _isDownload = true;
            CreateWebRequestObject().Load(url);
            return _download;
        }

        /// <summary>
        /// Creates the HtmlWeb object and initializes all event handlers. 
        /// </summary>
        private HtmlWeb CreateWebRequestObject()
        {
            HtmlWeb web = new HtmlWeb();
            web.UseCookies = true;
            web.PreRequest = new HtmlWeb.PreRequestHandler(OnPreRequest);
            web.PostResponse = new HtmlWeb.PostResponseHandler(OnAfterResponse);
            web.PreHandleDocument = new HtmlWeb.PreHandleDocumentHandler(OnPreHandleDocument);
            return web;
        }

        /// <summary>
        /// Event handler for HtmlWeb.PreRequestHandler. Occurs before an HTTP request is executed.
        /// </summary>
        protected bool OnPreRequest(HttpWebRequest request)
        {
            try
            {
                AddHeaders(request);
                AddCookiesTo(request);               // Add cookies that were saved from previous requests
                if (_isPost) AddPostDataTo(request, DoUrlEncodeFormParameterValues); // We only need to add post data on a POST request
            }
            catch (Exception e)
            {
                var foo = e;
            }
            return true;
        }

        /// <summary>
        /// adds headers to request
        /// </summary>
        /// <param name="request"></param>
        private void AddHeaders(HttpWebRequest request)
        {
            if (HeaderElements != null)
            {
                foreach (var item in HeaderElements)
                {
                    if (request.Headers.AllKeys.Contains<string>(item.Key))
                        continue;
                    switch (item.Key)
                    {
                        case "Accept":
                            request.Accept = item.Value;
                            break;
                        case "Content-Type":
                            request.ContentType = item.Value;
                            break;
                        case "Connection":
                            request.Connection = item.Value;
                            break;
                        case "Referer":
                            request.Referer = item.Value;
                            break;
                        case "Content-Length":
                            request.ContentLength = Int32.Parse(item.Value);
                            break;
                        case "User-Agent":
                            request.UserAgent = item.Value;
                            break;

                        default:
                            if (request.Headers.AllKeys.Contains(item.Key))
                            {
                                request.Headers[item.Key] = item.Value;
                            }
                            else
                            {
                                request.Headers.Add(item.Key, item.Value);
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for HtmlWeb.PostResponseHandler. Occurs after a HTTP response is received
        /// </summary>
        protected void OnAfterResponse(HttpWebRequest request, HttpWebResponse response)
        {
            SaveCookiesFrom(request, response); // Save cookies for subsequent requests
            Stream remoteStream = response.GetResponseStream();
            var sr = new StreamReader(remoteStream);

            if (response != null && _isDownload)
            {
                _download = sr.ReadToEnd();
            } else
            {
                LatestResponseBody = sr.ReadToEnd();
            }
        }

        protected string LatestResponseBody {get;set;}

        /// <summary>
        /// Event handler for HtmlWeb.PreHandleDocumentHandler. Occurs before a HTML document is handled
        /// </summary>
        protected void OnPreHandleDocument(HtmlDocument document)
        {
            SaveHtmlDocument(document);
        }

        /// <summary>
        /// Assembles the Post data and attaches to the request object
        /// </summary>
        private void AddPostDataTo(HttpWebRequest request, bool doUrlEncodeValues)
        {
            string payload = FormElements.AssemblePostPayload(doUrlEncodeValues);
            byte[] buff = Encoding.UTF8.GetBytes(payload.ToCharArray());
            request.ContentLength = buff.Length;
            //request.ContentType = "application/x-www-form-urlencoded";
            System.IO.Stream reqStream = request.GetRequestStream();
            reqStream.Write(buff, 0, buff.Length);
        }

        /// <summary>
        /// Add cookies to the request object
        /// </summary>
        private void AddCookiesTo(HttpWebRequest request)
        {
            if (Cookies != null && Cookies.Count > 0)
            {
                request.CookieContainer.Add(Cookies);
            }
        }

        /// <summary>
        /// Saves cookies from the response object to the local CookieCollection object
        /// </summary>
        private void SaveCookiesFrom(HttpWebRequest request, HttpWebResponse response)
        {
            if (request.CookieContainer.Count > 0 || response.Cookies.Count > 0)
            {
                if (Cookies == null)
                {
                    Cookies = new CookieCollection();
                }

                Cookies.Add(request.CookieContainer.GetCookies(request.RequestUri));
                Cookies.Add(response.Cookies);
            }
        }

        /// <summary>
        /// Saves the form elements collection by parsing the HTML document
        /// </summary>
        private void SaveHtmlDocument(HtmlDocument document)
        {
            _htmlDoc = document;
            FormElements = new FormElementCollection(_htmlDoc);
        }

        public string UserAgentString { get; set; }
    }


    public class HeaderElementCollection : Dictionary<string, string>
    {

    }


    /// <summary>
    /// Represents a combined list and collection of Form Elements.
    /// </summary>
    public class FormElementCollection : Dictionary<string, string>
    {
        /// <summary>
        /// Constructor. Parses the HtmlDocument to get all form input elements. 
        /// </summary>
        public FormElementCollection(HtmlDocument htmlDoc)
        {
            var inputs = htmlDoc.DocumentNode.Descendants("input");
            foreach (var element in inputs)
            {
                string name = element.GetAttributeValue("name", "undefined");
                string value = element.GetAttributeValue("value", "");

                if (!this.ContainsKey(name))
                {
                    if (!name.Equals("undefined"))
                    {
                        Add(name, value);
                    }
                }
            }
        }

        /// <summary>
        /// Assembles all form elements and values to POST. Also html encodes the values.  
        /// </summary>
        public string AssemblePostPayload(bool urlEncode = true)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var element in this)
            {
                string value = null;
                if (urlEncode)
                {
                    value = System.Web.HttpUtility.UrlEncode(element.Value);
                }
                else
                {
                    value = element.Value;
                }
                sb.Append("&" + element.Key + "=" + value);
            }
            return sb.ToString().Substring(1);
        }
    }




}
