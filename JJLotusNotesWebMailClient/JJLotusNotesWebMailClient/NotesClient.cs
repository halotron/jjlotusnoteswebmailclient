﻿using JJLotusNotesWebMailClient.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Security;
using System.Text;
using System.Web;

namespace JJLotusNotesWebMailClient
{
    public class NotesClient : INotesClient
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public int CheckNumberOfUnreadEmailsInInbox()
        {
            if (string.IsNullOrEmpty(NotesMailWebServerAddress) || NotesMailWebServerAddress.Length < 9)
            {
                throw new Exception("NotesMailWebServerAddress is not set");
            }
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                throw new Exception("username and password needs to be set");
            }

            try
            {
                BrowserSession session = new BrowserSession();

                session.UserAgentString = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)";

                var firstPage = session.Get(NotesMailWebServerAddress);

                session.HeaderElements.Add("User-Agent", "4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
                session.DoUrlEncodeFormParameterValues = false;

                session.FormElements["userName"] = UserName;
                session.FormElements["password"] = Password;
                session.FormElements["goUrl"] = HttpUtility.UrlEncode(NotesMailWebServerAddress);

                session.HeaderElements.Add("Accept", "image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
                session.HeaderElements.Add("Referer", NotesMailWebServerAddress);
                session.HeaderElements.Add("Accept-Language", "sv-SE");
                session.HeaderElements.Add("Content-Type", "application/x-www-form-urlencoded");
                session.HeaderElements.Add("Cache-Control", "no-cache");

                string response = session.Post(NotesMailWebServerAddress + "/ProgLogin");

                session.HeaderElements.Clear();
                session.FormElements.Clear();

                session.HeaderElements.Add("User-Agent", "4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");

                string bar = session.Get(NotesMailWebServerAddress + "/mail07/KSE23210000168PVM.nsf");


                var shimmerCookie = session.Cookies["ShimmerS"];
                var startOfString = ":M&N:";
                var endOfString = ";";


                var startIndex = shimmerCookie.Value.LastIndexOf(startOfString);

                var nonce = shimmerCookie.Value.Substring(startIndex + startOfString.Length);


                session.HeaderElements.Clear();
                session.FormElements.Clear();
                session.FormElements["s_FolderListNames"] = "FolderName";
                session.FormElements["FolderName"] = "(%24Inbox)";

                //session.HeaderElements.Add("x-ibm-inotes-nonce", "3F58660E76A429FD50598219AD5E0227");
                session.HeaderElements.Add("Referer", NotesMailWebServerAddress + "/mail07/KSE23210000168PVM.nsf/iNotes/Proxy/?OpenDocument&Form=l_ScriptFrame&l=sv&CR&MX&TS=20140119T180739,23Z&charset=ISO-8859-1&charset=ISO-8859-1&ua=ie");
                session.HeaderElements.Add("User-Agent", "4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");

                session.HeaderElements.Add("Accept", "*/*");
                session.HeaderElements.Add("Accept-Language", "sv");
                session.HeaderElements.Add("Cache-Control", "no-cache");

                session.HeaderElements.Add("x-ibm-inotes-nonce", nonce);
                //session.HeaderElements.Add("Connection", "Keep-Alive");


                session.DoUrlEncodeFormParameterValues = false;

                string response2 = session.Post(NotesMailWebServerAddress + "/mail07/KSE23210000168PVM.nsf/iNotes/Proxy/?EditDocument&Form=s_GetFolderUnreadCountJSON");

                var startString = "{\"($Inbox)\" : ";

                int nr = 0;

                if (response2.Contains(startString))
                {
                    var startIndex2 = response2.IndexOf(startString);
                    var endIndex = response2.IndexOf("}", startIndex);

                    startIndex2 += startString.Length;
                    var nrString = response2.Substring(startIndex2, endIndex - startIndex2);

                    nr = Int32.Parse(nrString);
                }


                var foo = response2;
                return nr;
            }
            catch (Exception)
            {
                return -1; //we swallow the error and hopes that it is temporary
            }

        }

        private string _userName;
        public string UserName
        {
            get
            {
                if (_userName == null)
                {
                    var store = IsolatedStorageFile.GetStore(IsolatedStorageScope.User |
    IsolatedStorageScope.Assembly | IsolatedStorageScope.Domain, null, null);
                    var fs = store.OpenFile("config.config", System.IO.FileMode.OpenOrCreate);


                    try
                    {
                        var currentUser = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                        if (!currentUser.Contains(" "))
                        {
                            _userName = currentUser;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                return _userName;
            }
            set
            {
                _userName = value;
                if (_userName != null)
                {
                    _userName = _userName.Trim();
                }
            }
        }

        public string Password { get; set; }

        private string _notesWebServer;
        public string NotesMailWebServerAddress
        {
            get
            {
                return _notesWebServer;
            }
            set
            {
                _notesWebServer = value;
                if (_notesWebServer != null)
                {
                    _notesWebServer = _notesWebServer.Trim();
                    if (_notesWebServer != null && !_notesWebServer.ToLowerInvariant().StartsWith("http"))
                    {
                        _notesWebServer = "https://" + _notesWebServer;
                    }
                }
            }
        }


        private int _waitNumberOfMinutes;
        public int WaitNumberOfMinutesBetweenUpdates
        {
            get
            {
                if (_waitNumberOfMinutes == 0)
                {
                    _waitNumberOfMinutes = 5;
                }
                return _waitNumberOfMinutes;
            }
            set
            {
                _waitNumberOfMinutes = value;
                if (value > 15)
                {
                    _waitNumberOfMinutes = 5;
                }
            }
        }


        public void ReadSettings()
        {
            try
            {
                using (var store = IsolatedStorageFile.GetStore(IsolatedStorageScope.User |
    IsolatedStorageScope.Assembly | IsolatedStorageScope.Domain, null, null))
                {
                    using (var fileStream = store.OpenFile("settings.xml", System.IO.FileMode.OpenOrCreate))
                    {
                        if (fileStream.Length > 0)
                        {
                            StreamReader sr = new StreamReader(fileStream);
                            var content = sr.ReadToEnd().Split('|');
                            UserName = content[0].Trim();
                            Password = content[1].Trim();
                            WaitNumberOfMinutesBetweenUpdates = Int32.Parse(content[2].Trim());
                            NotesMailWebServerAddress = content[3].Trim();
                        }
                    }
                }
            }
            catch { }
        }

        public void WriteSettings()
        {
            try
            {
                using (var store = IsolatedStorageFile.GetStore(IsolatedStorageScope.User |
    IsolatedStorageScope.Assembly | IsolatedStorageScope.Domain, null, null))
                {
                    using (var fileStream = store.OpenFile("settings.xml", System.IO.FileMode.OpenOrCreate))
                    {
                        StreamWriter sr = new StreamWriter(fileStream);
                        sr.Write(UserName + "|" + Password + "|" + WaitNumberOfMinutesBetweenUpdates + "|" + NotesMailWebServerAddress);
                        sr.Flush();
                    }
                }
            }
            catch { }
        }


        private int _popupDurationMs;
        public int PopupDuration
        {
            get
            {
                if (_popupDurationMs < 500)
                {
                    _popupDurationMs = 500;
                }
                return _popupDurationMs;
            }
            set
            {
                if (value < 500)
                {
                    _popupDurationMs = 500;
                }
                else
                {
                    _popupDurationMs = value;
                }
            }
        }
    }
}
