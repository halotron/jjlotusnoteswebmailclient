﻿using Hardcodet.Wpf.TaskbarNotification;
using JJLotusNotesWebMailClient;
using JJLotusNotesWebMailClient.Interfaces;
using NAppUpdate.Framework;
using NAppUpdate.Framework.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Timers;
using System.Windows;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Controller.I.StartTimer();
            var updManager = UpdateManager.Instance;

            var sigFeedReader = new NAppUpdate.Framework.FeedReaders.NauXmlFeedReaderSigned();
            sigFeedReader.PublicKeys = new string[]{"<RSAKeyValue><Modulus>oSaXTOo1b4TdMxzr7ROhAjmOpb2NXNmnNHX2a1Gh9l1V8wgf9X2aWVIotzhClNShMrvcvPItf9cy8P8FggcFxGyckqYJWhBTWVOiFyO9Y8xWWL505wQuoTNqvCajXUKBiYf0mlQhxBJacQ2GxECciIfb8/UkKqBPs8OJaZWUzX4V0Oy0CVso7qx6fIAzt5Bb2j5Rn72vGRPrnKIU8hm7WAtiBMFU/ub3GvM2Z8a9joHxFLV+7CfYsiLepLDT1QepuNN8dlUu2kKPlHNKLqRCdxjISMGQvsFiD3fOCEojp5zvIxMvwpnbZNCQwI9l251s63LkYgL/7BNDtL3Zavrasw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>"};

            updManager.UpdateFeedReader = sigFeedReader;
            updManager.UpdateSource = PrepareUpdateSource();
            updManager.ReinstateIfRestarted();
            CheckForUpdates();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Controller.I.Dispose();
            base.OnExit(e);
        }

        private NAppUpdate.Framework.Sources.IUpdateSource PrepareUpdateSource()
        {
            var source = new NAppUpdate.Framework.Sources.SimpleWebSource("http://www.knacka.se/software_updates/jjlotusnoteswebmailclient/updates.xml");
            return source;
        }

        private bool applyUpdates;

        private void CheckForUpdates()
        {
            UpdateManager updManager = UpdateManager.Instance;

            updManager.BeginCheckForUpdates(asyncResult =>
            {
                try
                {

                    Action showUpdateAction = ShowUpdateWindow;

                    if (asyncResult.IsCompleted)
                    {
                        ((UpdateProcessAsyncResult)asyncResult).EndInvoke();

                        // No updates were found, or an error has occured. We might want to check that...
                        if (updManager.UpdatesAvailable == 0)
                        {
                            //MessageBox.Show("All is up to date!");
                            return;
                        }
                    }

                    applyUpdates = true;

                    if (Dispatcher.CheckAccess())
                        showUpdateAction();
                    else
                        Dispatcher.Invoke(showUpdateAction);
                }
                catch (Exception)
                {
                    var foo = updManager.Logger.LogItems;
                }
            }, null);
        }

        private void ShowUpdateWindow()
        {
            UpdateManager.Instance.BeginPrepareUpdates((x) =>
                {
                    try
                    {
                        UpdateManager.Instance.ApplyUpdates(true);
                    }
                    catch (Exception)
                    {
                    }
                    UpdateManager.Instance.CleanUp();
                }, null);
        }

    }
}

