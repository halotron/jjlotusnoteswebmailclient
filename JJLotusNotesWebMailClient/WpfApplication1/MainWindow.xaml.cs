﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public MainWindow()
        {
            InitializeComponent();

            ServerUriBox.Text = Controller.I.NotesClient.NotesMailWebServerAddress;
            HowManyMinutesBox.Text = Controller.I.NotesClient.WaitNumberOfMinutesBetweenUpdates.ToString();
            UserNameBox.Text = Controller.I.NotesClient.UserName;
            PasswordBox.Password = Controller.I.NotesClient.Password;
            PopupDurationBox.Text = Controller.I.NotesClient.PopupDuration.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.I.NotesClient.NotesMailWebServerAddress = ServerUriBox.Text;
            try
            {
                Controller.I.NotesClient.WaitNumberOfMinutesBetweenUpdates = Int32.Parse(HowManyMinutesBox.Text);
            } catch
            { }
            Controller.I.NotesClient.UserName = UserNameBox.Text;
            Controller.I.NotesClient.Password = PasswordBox.Password;

            try
            {
                Controller.I.NotesClient.PopupDuration = Int32.Parse(PopupDurationBox.Text);
            }
            catch {
                Controller.I.NotesClient.PopupDuration = 1200;
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
