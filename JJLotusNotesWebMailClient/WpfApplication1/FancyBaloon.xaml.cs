﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for FancyBaloon.xaml
    /// </summary>
    public partial class FancyBaloon : UserControl
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public FancyBaloon()
        {
            InitializeComponent();
            tb.Text = Controller.I.CurrentEmailCount + " new email(s)";
        }
    }
}
