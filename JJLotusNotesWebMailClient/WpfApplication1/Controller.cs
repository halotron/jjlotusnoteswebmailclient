﻿using Hardcodet.Wpf.TaskbarNotification;
using JJLotusNotesWebMailClient;
using JJLotusNotesWebMailClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Threading;

namespace WpfApplication1
{
    public class Controller : IDisposable
    {
        private NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private Controller()
        {
            if(NotifyIcon == null)
            {
                throw new Exception();
            }
            NotesClient.ReadSettings();
        }
        private TaskbarIcon _notifyIcon;
        private static object lockObject = new object();
        private static volatile Controller _i;
        public static Controller I
        {
            get
            {
                if(_i == null)
                {
                    lock (lockObject)
                    {
                        if (!_isDisposed && _i == null) //just for the fun of it, we do a double checked lock
                        {
                            _i = new Controller();
                        }
                    }
                }
                return _i;
            }
        }
        private static bool _isDisposed;
        public void Dispose()
        {
            NotesClient.WriteSettings();
            _timer = null;
            _client = null;
            NotifyIcon = null;
            _isDisposed = true;
            _i = null;
        }

        private INotesClient _client;
        public INotesClient NotesClient
        {
            get
            {
                if (_client == null)
                {
                    _client = new NotesClient();
                }
                return _client;
            }
        }

        private DispatcherTimer _timer;
        private int _oldNumberOfEmails;

        public void StartTimer()
        {
            if(!_isDisposed &&_timer == null)
            {

                _timer = new DispatcherTimer();
                _timer.Tick += (x, y) =>
                    {
                        int nrEmails = NotesClient.CheckNumberOfUnreadEmailsInInbox();
                        if(nrEmails == -1)
                        {
                            //we have an error, so we ignore this temporarily
                        } else if(nrEmails == 0)
                        {
                            _oldNumberOfEmails = 0;
                        }
                        else
                        {
                            if(nrEmails != _oldNumberOfEmails)
                            {
                                int temp = _oldNumberOfEmails;
                                _oldNumberOfEmails = nrEmails;

                                if(nrEmails > temp)
                                {
                                    //NotifyIcon.ShowBalloonTip("YOU GOT MAIL", nrEmails + " email(s)", BalloonIcon.Info);

                                    NotifyIcon.ShowCustomBalloon(new FancyBaloon(), System.Windows.Controls.Primitives.PopupAnimation.Slide, 2100);

                                }
                            }
                        }
                    };
            }
            _timer.Interval = TimeSpan.FromMinutes(NotesClient.WaitNumberOfMinutesBetweenUpdates);
            _timer.Start();
        }

        public int CurrentEmailCount {
            get
            {
                return _oldNumberOfEmails;
            }
        }

        public void StopTimer()
        {
            _timer.Stop();
        }

        public TaskbarIcon NotifyIcon
        {
            get
            {
                if(_notifyIcon == null && !_isDisposed)
                {
                    _notifyIcon = (TaskbarIcon)App.Current.FindResource("NotifyIcon");
                }
                return _notifyIcon;
            }
            private set
            {
                _notifyIcon = value;
            }
        }
    }
}
